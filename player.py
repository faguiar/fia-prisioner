import random

class Player(object):
	ALLY = True
	BETRAY = False
	
	@staticmethod
	def vote():
		"""
			Retorna Player.ALLY ou Player.BETRAY
		"""
		
		return random.choice([Player.ALLY,Player.BETRAY])
	
	@staticmethod
	def change_side(me,neighbours):
		"""
			me: objeto PlayerState, com classe e pontuacao do jogador em si
			neighbours: Lista de PlayerState, com classes e pontuacoes de todos seus vizinhos
		
			Retorna classe que o jogador quer ser no proximo turno, ou None caso ele nao queira trocar de tipo
			
			Retornar sua propria classe eh valido tambem		
		"""
		scoreMax = me.playerScore
		side = me.playerClass
		for p in neighbours:
			if (p.playerScore >= scoreMax):
				side = p.playerClass
				scoreMax = p.playerScore
		
		return side

class PlayerState(object):
	playerClass = Player
	playerScore = 0
	
	def __init__(self,c,s):
		self.playerClass = c
		self.playerScore = s
