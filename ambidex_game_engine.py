import random
import copy
from player import Player,PlayerState

class AmbidexGameEngine(object):
	def __init__(self,width,height,players,aa=2,ab=-2,ba=3,bb=0):
		"""
			width: Numero de colunas do mapa em grid
			height: Numero de linhas do mapa em grid
			
			players: Lista de classes que herdam de Player
			
			aa: Pontos que ally ganha sobre ally
			ab: Pontos que ally ganha sobre betray
			ba: Pontos que betray ganha sobre ally
			bb: Pontos que betray ganha sobre betray
		"""
		
		self.width = width
		self.height = height
		self.players = players
		
		self.points = {
			Player.ALLY: {
				Player.ALLY: aa,
				Player.BETRAY: ab,
			},
			Player.BETRAY: {
				Player.ALLY: ba,
				Player.BETRAY: bb,
			},
		}
	
	@staticmethod
	def create_with_b(width,height,players,b):
		"""
			width: Numero de colunas do mapa em grid
			height: Numero de linhas do mapa em grid
			
			players: Lista de classes que herdam de Player
			
			b: Pontos que betray ganha sobre ally
		"""
		return AmbidexGameEngine(width,height,players,1,0,b,0)
	
	def get_width(self):
		return self.width
	
	def get_height(self):
		return self.height
	
	def get(self,x,y):
		"""
			Retorna tupla com duas classes que herdam de Player, ou None na primeira iteracao.
		"""
		
		return (self.olf_fields[x][y].playerClass,self.fields[x][y].playerClass,)
	
	def set(self,x,y,playerClass):
		self.fields[x][y].playerClass = playerClass
	
	def new_game(self):
		"""
			Inicia um novo jogo. Pode ser chamado varias vezes.
		"""
		
		self.fields = [[PlayerState(random.choice(self.players),0) for i in xrange(self.height)] for j in xrange(self.width)]
		
		self.olf_fields = copy.deepcopy(self.fields)
		
		pass
	
	def __get_field(self,x,y):
		if(x < 0 or y < 0):
			return None
		
		if(x >= len(self.fields) or y >= len(self.fields[0])):
			return None
		
		return self.fields[x][y]
	
	def next_step(self):
		"""
			Roda uma iteracao do jogo.
		"""
		
		self.olf_fields = copy.deepcopy(self.fields)
		
		for x in xrange(self.get_width()):
			for y in xrange(self.get_height()):
				player = self.__get_field(x,y)
				
				neighbours = [
					(x-1,y-1),
					(x-1,y+1),
					(x+1,y-1),
					(x+1,y+1),
					(x-1,y  ),
					(x+1,y  ),
					(x  ,y-1),
					(x  ,y+1),
				]
				
				for e in neighbours:
					enemy = self.__get_field(*e)
					
					if(enemy != None):
						p_vote = player.playerClass.vote()
						e_vote = enemy.playerClass.vote()
						
						player.playerScore += self.points[p_vote][e_vote]
						enemy.playerScore += self.points[e_vote][p_vote]
		
		for x in xrange(self.get_width()):
			for y in xrange(self.get_height()):
				player = self.__get_field(x,y)
				
				neighbours = [
					(x-1,y-1),
					(x-1,y  ),
					(x-1,y+1),
					(x  ,y-1),
					(x  ,y+1),
					(x+1,y-1),
					(x+1,y  ),
					(x+1,y+1),
				]
				
				neighbours = [self.__get_field(*i) for i in neighbours if self.__get_field(*i) != None]
				
				player.playerClass = player.playerClass.change_side(player,neighbours)
