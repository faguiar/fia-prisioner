#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

from player import *
from behaviours import *
from pygame_updater import *
from ambidex_game_engine import *
import sys
import re
import math

class ambidex_game(pygame_updater):
	def __init__(self, *args, **kwargs):
		default = kwargs.get("width",kwargs.get("height",20))
		self.CC = (0,0,255)
		self.DD = (255,0,0)
		self.DC = (255,255,0)
		self.CD = (0,255,0)
		self.w = kwargs.pop("width",default)
		self.h = kwargs.pop("height",default)
		self.b = kwargs.pop("b",1.8)
		
		super(ambidex_game,self).__init__(*args,**kwargs)
		
		self.start()
	
	def start(self):
		self._grid = self.grid(self.w,self.h)
		#self.engine = AmbidexGameEngine.create_with_b(self.w, self.h, [Mane,Mane,Mane,Mane,Mane,Mane,Mane,Mane,Mane, FilhoDaPuta], self.b)
		self.engine = AmbidexGameEngine.create_with_b(self.w, self.h, [Mane], self.b)
		
		self.engine.new_game()
		
		self.engine.set(int(math.floor(self.w/2.0)), int(math.floor(self.h/2.0)), FilhoDaPuta)
		
		self.engine.next_step()
		return
		
	
	def update(self,delta_time):
		self.engine.next_step()
		
		
		#print self.engine.get_height(), self.engine.get_width()
		
		for x in xrange(self.engine.get_height()):
			for j in xrange(self.engine.get_width()):
				rect = [i for i in
					self._grid.get_position_at(x,j,-1,-1) +
					(self._grid.cell_width(), self._grid.cell_height())
				]
				
				#print rect

				typeofplayer = self.engine.get(x,j)
				#print typeofplayer
				#print typeofplayer[0] == Mane
				#print type(typeofplayer[1])
				if(typeofplayer[0] == Mane):
					if(typeofplayer[1] == Mane):
						pygame.draw.rect(self.screen, self.CC, rect)
					else:
						pygame.draw.rect(self.screen, self.CD, rect)
				elif(typeofplayer[0] == FilhoDaPuta):
					if(typeofplayer[1] == FilhoDaPuta):
						pygame.draw.rect(self.screen, self.DD, rect)
					else:
						pygame.draw.rect(self.screen, self.DC, rect)
				else:
					pass
				#pygame.time.delay(1)

		return
	
			
def main():
	args = {}
	benchmark = False
	
	for i in sys.argv[1:]:
		if(re.match("width=(\d+)",i)):
			args["width"] = int(re.match("width=(\d+)",i).group(1))
		elif(re.match("height=(\d+)",i)):
			args["height"] = int(re.match("height=(\d+)",i).group(1))
		elif(re.match("b=(\d+(.\d+)?)$",i)):
			args["b"] = float(re.match("b=(\d+(.\d+)?)$",i).group(1))
		elif(i=="benchmark"):
			benchmark = True
	
	if(benchmark):
		pass
		# ant_game_benchmark(**args)
		# 
		# Chamada a classe benchmark, que roda o jogo sem front para geracao de estatiticas
	else:
		# 
		# Geracao da classe de front que roda o jogo
		game = ambidex_game(640, 480, **args)
		game.loop_forever();

if __name__ == "__main__":
    main()

