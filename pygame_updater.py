import pygame
import sys
import time

class grid(object):
	def __init__(self,c,r,w,h):
		self.columns = c
		self.rows = r
		self.width = w
		self.height = h
	
	def cell_width(self):
		return self.width/self.columns
	
	def cell_height(self):
		return self.height/self.rows
	
	def get_position_at(self,x,y,cx=0,cy=0):
			return (
			int((self.cell_width())*(x+1+float(cx+1 / 2))),
			int((self.cell_height())*(y+1+float(cy+1 / 2))),
		)
	
	def get_center_at(self,x,y):
		return (
			int((self.cell_width())*(x+0.5)),
			int((self.cell_height())*(y+0.5)),
		)

class pygame_updater(object):
	BLACK = (0,0,0)
	
	def __init__(self,w,h):
		self.width = w
		self.height = h
	
	def size(self):
		return (self.width, self.height,)
	
	def grid(self,c,r):
		return grid(c,r,self.width,self.height)
	
	def loop_forever(self):
		self.screen = screen = pygame.display.set_mode(self.size())
		
		last_update = time.time()
		
		self.start()
		
		while True:
			for event in pygame.event.get():
				if event.type == pygame.QUIT: sys.exit()
			
			screen.fill(self.BLACK)
			
			this_update = time.time()
			self.update(this_update - last_update);
			last_update = this_update
			
			pygame.display.flip()
	
	def start(self):
		pass
		
	def update(self,delta_time):
		pass
